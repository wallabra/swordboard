const path = require('path');

const VueLoaderPlugin = require('vue-loader/lib/plugin');



module.exports = {
    mode: "development",
    devtool: "inline-source-map",
    entry: "./src/web/app.ts",

    output: {
        path: path.resolve(__dirname, 'web'),
        filename: "bundle.js"
    },

    resolve: {
        extensions: [".ts", ".tsx", '.js', '.vue', '.json'],

        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        },
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: "ts-loader",
                        options: {
                            appendTsSuffixTo: [/\.vue$/]
                        },
                    },
                ],
            },

            {
                test: /\.(png|jpe?g|gif|ogg)$/i,
                use: [
                    {
                        loader: 'url-loader'
                    },
                ],
            },

            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },

            /*
            {
                test: /pixi\.js/,
                loader: 'expose-loader?PIXI'
            }
            */
        ]
    },

    plugins: [
        new VueLoaderPlugin(),
    ]
};