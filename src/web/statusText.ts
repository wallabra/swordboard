import * as PIXI from 'pixi.js';
import Vue from 'vue';



export class StatusText {
    public content: string;
    public x: number;
    public y: number;
    public text: PIXI.Text;
    public duration: number = 0.8;

    constructor(content: string, x: number, y: number, ...args: any[]) {
        this.content = content;
        this.x = x;
        this.y = y;

        this.text = new PIXI.Text(content);

        this.text.x = x;
        this.text.y = y;

        this.apply(...args)
    }

    show(app: Vue) {
        let renderer: any = app.$refs.renderer;
        let pixi: PIXI.Application = renderer.pixi;

        let rts = Math.sqrt(renderer.scale);

        this.text.scale.x = rts * 0.65;
        this.text.scale.y = rts * 0.65;

        pixi.stage.addChild(this.text);

        setInterval(() => {
            this.text.y -= (0.05 / this.duration) * 35;
            this.text.alpha -= (0.05 / this.duration);
        }, 50);

        setTimeout(() => {
            pixi.stage.removeChild(this.text);
            this.text.destroy();
        }, this.duration * 1000);
    }

    apply(...args: any[]) {}
}

export class DamageText extends StatusText {
    public type: string = "";

    constructor(damage: number, type: string, x: number, y: number) {
        super(''+damage, x, y, type);
    }

    apply(type: string) {
        this.type = type;

        if (this.type === 'critical')
            this.text.style.fill = '#FF8B';

        else if (this.type === 'defense')
        this.text.style.fill = '#F5C8';
    }
}

export class ChatText extends StatusText {
    apply() {
        this.duration = 5;
        this.text.style.fill = '#5AFB';
    }
}