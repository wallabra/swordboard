import { socket, board, checker } from './net';
import { Player, BoardObject, BoardLiving } from '../index';
import Vue from 'vue';

import RootApp from './components/rootapp.vue';

// Sounds
import sclamp from './sounds/sclamp.ogg';
import woodBang from './sounds/wood bang.ogg';
import step from './sounds/step.ogg';
import * as Status from './statusText';



let pid: string | null = null;

Vue.config.productionTip = false;

let app = new Vue({
    el: '#root',
    components: { rootapp: RootApp }
});

(window as any).restart = function() {
    let app = new Vue({
        el: '#root',
        components: { rootapp: RootApp }
    }); 
};

const rootApp: Vue = app.$refs.app as Vue;
let player: Player | null = null;


function updateObjects() {
    (rootApp as any).objects = Array.from((window as any).board.objects.values());
}

function updateObjectLabels() {
    (window as any).vueObjs.forEach((vo: any, obj: BoardObject) => {
        vo.color = (obj instanceof BoardLiving && obj.player) ? '#85F' : '#CCAf';
    });
}

function vueAddObject(obj: BoardObject) {
    (rootApp.$refs.renderer as any).addObject(obj);
}

function vueUpdate() {
    (rootApp.$refs.renderer as any).onUpdate();
}

function vueRemoveObject(obj: BoardObject) {
    (rootApp.$refs.renderer as any).removeObject(obj);
}


checker.on('logged in', () => {
    (rootApp as any).dead = false;
    
    console.log('Logged in!');

    vueUpdate();
    updateObjectLabels();

    (rootApp as any).myTurn = (board.players.get(board.turns[board.turnIndex]) === player);
    (rootApp as any).numPlayers = Array.from(board.players.values()).filter((p) => validPlayer(p)).length;

    (rootApp as any).playerX = player!.object!.pos.x;
    (rootApp as any).playerY = player!.object!.pos.y;
    (rootApp as any).logged = true;

    player!.object!.on('move', () => {
        (rootApp as any).playerX = player!.object!.pos.x;
        (rootApp as any).playerY = player!.object!.pos.y;
    });

    player!.object!.on('death', (instig) => {
        (rootApp as any).dead = true;
        (rootApp as any).logged = false;

        if (board.objects.has(instig) && board.objects.get(instig)! instanceof BoardLiving) {
            console.log('Instigator: ', (board.objects.get(instig)! as BoardLiving).name);
            (rootApp as any).$refs.login.enemyName = (board.objects.get(instig)! as BoardLiving).name;
        }

        else {
            (rootApp as any).$refs.login.enemyName = '';
        }
    });

    (rootApp as any).myTurn = (board.players.get(board.turns[board.turnIndex]) === player);
});

checker.on('clean', () => {
    (rootApp as any).logged = false;
    (window as any).player = null;

    vueUpdate();
    updateObjects();
});

checker.on('player', (play) => {
    if (!pid || play.id === pid) {
        pid = play.id;

        (window as any).player = play;
        player = play;
    }

    else {
        socket.emit('DATA', '!RESTORE ' + pid + '\n');
    }
});

socket.on('disconnect', () => {
    (rootApp as any).disconnected = true;
});

socket.on('error', (err: Error) => {
    console.error(err);
    (rootApp as any).disconnected = true;
});

socket.on('reconnect', () => {
    (rootApp as any).disconnected = false;
});

socket.on('connect', () => {
    (rootApp as any).disconnected = false;
});

socket.on('chat', (objId: string, str: string) => {
    if (board.objects.has(objId)) {
        let obj = board.objects.get(objId)!;

        let playerX = 0, playerY = 0;

        if (player == null || player!.object != null) {
            playerX = player!.object!.pos.x;
            playerY = player!.object!.pos.y;
        }

        let viewWidth = (rootApp.$refs.view as any).clientWidth;
        let viewHeight = (rootApp.$refs.view as any).clientHeight;

        let size = 40;

        new Status.ChatText(
            str,
            ((obj.pos.x - playerX - 2) * (rootApp as any).scale * size + viewWidth / 2),
            ((obj.pos.y - playerY - 0.75) * (rootApp as any).scale * size + viewHeight / 2),
        ).show(rootApp);
    }
});

function volumeFor(o: BoardObject) {
    let playerX = 0, playerY = 0;

    if (player == null || player!.object != null) {
        playerX = player!.object!.pos.x;
        playerY = player!.object!.pos.y;
    }

    let dist = ((playerX - o.pos.x) ** 2 + (playerY - o.pos.y) ** 2);
    let vol  = 1 / (1 + dist);
    
    return {
        volume:   vol,
    //  distance: dist ** 1/2,
    };
}

checker.on('health', (h) => {
    console.log('Setting health to', h);

    setTimeout(() => {
        (rootApp.$refs.s_hel as any).amount = h;
    }, 200);
});

checker.on('stats', (stt) => {
    console.log('Setting stats');

    setTimeout(() => {
        (rootApp.$refs.s_att as any).amount = stt.attack;
        (rootApp.$refs.s_def as any).amount = stt.defense;
        (rootApp.$refs.s_agl as any).amount = stt.agility;
        (rootApp.$refs.s_lck as any).amount = stt.luck;
        (rootApp.$refs.s_spd as any).amount = stt.speed;
        (rootApp.$refs.s_acc as any).amount = stt.accuracy;
    }, 200);
});

checker.on('points', (pts) => {
    setTimeout(() => {
        (rootApp as any).points = pts;
    }, 200);
});

checker.on('object event', () => {
    vueUpdate();
    updateObjects();
});

let notim = 0;

board.on('new object', (o: BoardObject) => {
    if (notim != 0)
        clearInterval(notim);

    setTimeout(() => {
        vueAddObject(o);
    }, 500);
    
    notim = setTimeout(() => {
        updateObjects();
        updateObjectLabels();
        notim = 0;
        
    }, 100) as any;

    o.on('move', () => {
        if (o instanceof BoardLiving) {
            let { volume } = volumeFor(o);

            if (volume > 0.05) {
                let audio = new Audio(step);
                audio.volume = volume;
                audio.play();
            }
        }
    });

    if (o instanceof BoardLiving) {
        o.on('death', () => {
            let { volume } = volumeFor(o);
            
            if (volume > 0.05) {
                let audio = new Audio(sclamp);
                
                if (o.player) {
                    audio.playbackRate = 0.3;
                }
                
                audio.volume = volume;
                audio.play();
            }
        });
    }

    o.on('damage', (dmg: string, type: string) => {
        let playerX = 0, playerY = 0;

        if (player == null || player!.object != null) {
            playerX = player!.object!.pos.x;
            playerY = player!.object!.pos.y;
        }
        
        let viewWidth = (rootApp.$refs.view as any).clientWidth;
        let viewHeight = (rootApp.$refs.view as any).clientHeight;

        let size = 40;

        let audio = new Audio(woodBang);
        let { volume } = volumeFor(o);

        if (volume > 0.05) {
            audio.volume = volume;
            audio.play();
        }

        new Status.DamageText(
            +dmg, type,
            ((o.pos.x - playerX + Math.random() * 0.75 - 0.75) * (rootApp as any).scale * size + viewWidth / 2),
            ((o.pos.y - playerY) * (rootApp as any).scale * size + viewHeight / 2 - 25),
        ).show(rootApp);
    });
});

board.on('deleted object', (o) => {
    vueRemoveObject(board.objects.get(o)!);
    updateObjects();
});

function validPlayer(player?: Player) {
    return player != null && player.object != null && !player.frozen;
}

checker.on('turn', () => {
    (rootApp as any).myTurn = (board.players.get(board.turns[board.turnIndex]) === player);
    (rootApp as any).numPlayers = Array.from(board.players.values()).filter((p) => validPlayer(p)).length;

    if (validPlayer(board.players.get(board.turns[board.turnIndex])))
        (rootApp as any).currentTurn = board.players.get(board.turns[board.turnIndex])!.object!.name;

    else
        (rootApp as any).currentTurn = null;
});


//board.on('new object', app.$forceUpdate(function(){}));

(window as any).board   = board;
(window as any).app     = app;
(window as any).vueObjs = new Map();
