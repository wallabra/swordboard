import { LivingStatsOptions, ProjectileStatsOptions, BoardLiving, Board, Position } from './index';
import monsters from './monsters.json';



export interface MonsterDef {
    name: string;
    sprite?: string;
    health?: number;
    stats?: LivingStatsOptions;
    projectiles?: ProjectileStatsOptions;
    rarity?: number;
};


export let monsterDefs: MonsterDef[] = monsters;


export function pickMonsterDef() {
    let maxRarity = 1;

    for (let i = 0; i < monsterDefs.length; i++) {
        if ((monsterDefs[i].rarity || 1) > maxRarity)
            maxRarity = (monsterDefs[i].rarity || 1);
    }

    let total = 0;

    for (let i = 0; i < monsterDefs.length; i++) {
        total += maxRarity - (monsterDefs[i].rarity || 1) + 1;
    }

    let pick = Math.random() * total;
    let cur = 0;

    for (let i = 0; i < monsterDefs.length; i++) {
        cur += maxRarity - (monsterDefs[i].rarity || 1) + 1;

        if (cur > pick)
            return monsterDefs[i];
    }

    throw new Error("No monster could be picked!");
}

export function buildMonster(board: Board) {
    let stuff = {};
    Object.assign(stuff, pickMonsterDef());

    (stuff as any).pos = new Position({
        x: Math.round((Math.random() ** 2) * 200 * (Math.random() > 0.5 ? 1 : -1)),
        y: Math.round((Math.random() ** 2) * 200 * (Math.random() > 0.5 ? 1 : -1)),
    });

    return new BoardLiving(board, stuff);
}