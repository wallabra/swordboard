import * as Swordboard from './index';
import { Server } from './server';
import readline from 'readline';
import { buildMonster } from './monster';



const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

let game = new Server({
    port: isNaN(+process.argv[2]) ? +process.argv[2] : undefined
});
(global as any).game = game;


function executeCommand(cmd: string, args: string[]) {
    if (cmd.toUpperCase() === 'CREATE') {
        let type = args[0];
        let options = args.slice(1).join(' ') ? JSON.parse(args.slice(1).join(' ')) : {};

        let t = (Swordboard as any)[type];

        if (t.prototype instanceof Swordboard.BoardObject) {
            let type: typeof Swordboard.BoardObject = t;

            let e = new type(game.board, options);
            
            console.log(`Created ${type.name} of ID ${e.id}.`);
        }
    }

    if (cmd.toUpperCase() === 'SPAWNEVERY') {
        let secs = +args[0];
        
        if (!isNaN(secs)) {
            setInterval(() => {
                let mon = buildMonster(game.board);
                console.log(`\rSpawned ${mon.name} (${mon.id}) at x=${mon.pos.x},y=${mon.pos.y}...`);
                process.stderr.write('> ');
            }, secs * 1000);

            console.log(`Spawning monsters every ${secs} seconds...`);
        }        
    }

    if (cmd.toUpperCase() === 'SPAWN') {
        let amount = isNaN(+args[0]) ? 1 : +args[0];

        for (let i = 0; i < amount; i++) {
            let mon = buildMonster(game.board);
            console.log(`Spawned ${mon.name} (${mon.id}) at x=${mon.pos.x},y=${mon.pos.y}...`);
        }
    }

    if (cmd.toUpperCase() === 'KILLALL') {
        let killCount = 0;

        game.board.objects.forEach((obj) => {
            if (obj instanceof Swordboard.BoardLiving && obj.name.toUpperCase() === args.join(' ').toUpperCase()) {
                obj.die();
                killCount++
            }
        });

        console.log(`Killed ${killCount} objects of type ${args[0]}.`)
    }
}

setTimeout(() => {
    rl.on('line', (line) => {
        let tok = line.split(' ');
        
        if (tok) {
            executeCommand(tok[0], tok.slice(1).filter((t) => t));
        }

        process.stderr.write('> ');
    });
}, 1000);