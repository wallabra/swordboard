import express from 'express';
import { Express } from 'express';
import SocketIO from 'socket.io';
import { Socket } from 'socket.io';
import { createServer } from 'http';
import { Board, Player, BoardOptions } from './index';
import * as path from 'path';



export class Server {
    public board: Board;
    public clientPlayers: Map<Socket, Player>;

    private server: Express;
    private io: SocketIO.Server;

    parseCmd(client: Socket, cmd: string) {
        let parts = cmd.split(' ');
        let command = parts[0];
        let args = parts.slice(1);

        console.log('\rGot ' + command);
        process.stderr.write('> ');

        if (!this.clientPlayers.has(client)) {
            if (command.toUpperCase() === '!RESTORE' && args.length > 0) {
                let p = this.board.players.get(args[0])!;

                if (p.frozen) {
                    this.clientPlayers.set(client, p);
                    p.unfreeze();
                    return true;
                }

                return false;
            }

            else
                return false;
        }

        else {
            let player = this.clientPlayers.get(client);
            player!.command(command, args);
            return false;
        }
    }

    constructor(boardOptions: BoardOptions = {}) {
        this.board = new Board(boardOptions);

        this.server = express();

        let http = createServer(this.server);

        this.server.use(express.static(path.join(__dirname, '/../web')));
        this.io = SocketIO(http, {
            pingInterval: 8000,
            pingTimeout: 15000,
        });

        this.io.on('connection', (socket) => {
            socket.emit('data', 'CLEAN\n');
            
            console.log(`\r+++ Client connected to board game from ${socket.client.conn.remoteAddress}`);
            process.stderr.write('> ');

            let buffer = '';
            let play = new Player(this.board);
            this.board.addPlayer(play);
            this.clientPlayers.set(socket, play);

            socket.on('data', (data: Buffer) => {
                let lines = (buffer + data.toString('utf-8')).split('\n');
                buffer = lines.slice(-1)[0];

                lines.slice(0, -1).forEach((cmd) => {
                    if (this.parseCmd(socket, cmd)) {
                        if (play) {
                            play.remove();
                        }
                            
                        play = this.clientPlayers.get(socket)!;
                        socket.emit('data', `ISPLAY ${play.id}\n`);
                    }
                });
            });

            socket.on('chat', (objId: string, str: string) => {
                this.io.emit('chat', objId, str);
            });


            socket.on('disconnect', (reason) => {
                console.log(`\r--- Client from ${socket.client.conn.remoteAddress} left (${reason})`);
                process.stderr.write('> ');
                
                play.remove();
            });

            this.board.objects.forEach((o) => {
                socket.emit('data', 'NEWOBJ ' + o.json() + '\n');
            });

            socket.emit('data', `ISPLAY ${play.id}\n`);
            socket.emit('data', 'TURNS ' + this.board.turnIndex + ' ' + JSON.stringify(this.board.turns) + '\n');

            play.on('login', (name) => {
                this.io.emit('data', `ISOBJC ${play.id} ${play.object!.id}\n`);
            });
        });

        this.server.on('error', (err) => {
            console.log(err);
        });

        http.listen(boardOptions.port || 8080, () => {
            console.log('Listening on port 8080.');
        });

        this.clientPlayers = new Map();

        this.board.objects.forEach((obj) => {
            obj.on('__NET_EVENT__', (id: string, args: any[]) => {
                this.objectNetEmit(id, args);
            });
        });

        this.board.on('new object', (obj) => {
            obj.on('__NET_EVENT__', (id: string, args: any[]) => {
                this.objectNetEmit(id, args);
            });
        });

        this.board.players.forEach((play) => {
            play.on('__NET_EVENT__', (id: string, args: any[]) => {
                this.playerNetEmit(id, args);
            });
        });

        this.board.on('new player', (play) => {
            play.on('__NET_EVENT__', (id: string, args: any[]) => {
                this.playerNetEmit(id, args);
            });
        });

        this.board.on('player login', (play) => {
            this.io.emit('data', 'TURNS ' + this.board.turnIndex + ' ' + JSON.stringify(this.board.turns) + '\n');
        });

        this.board.on('post deleted object', (oid) => {
            this.io.emit('data', 'DELOBJ ' + oid + '\n');
        });

        this.board.on('player removed', (play) => {
            this.io.emit('data', 'TURNS ' + this.board.turnIndex + ' ' + JSON.stringify(this.board.turns) + '\n');
        });

        this.board.on('turn', () => {
            this.io.emit('data', 'TURNS ' + this.board.turnIndex + ' ' + JSON.stringify(this.board.turns) + '\n');
        });
    }

    objectNetEmit(objId: string, args: any[]) {
        if (args[0] === 'ADD') {
            this.io.emit('data', 'NEWOBJ ' + this.board.objects.get(objId)!.json() + '\n');
        }

        else {
            this.io.emit('data', `OBJCEV ${objId} ${args[0]} ${JSON.stringify(args.slice(1))}\n`);
        }
    }

    playerNetEmit(playId: string, args: any[]) {
        if (args[0] === 'ADD') {
            this.io.emit('data', 'NEWPLY ' + playId + '\n');
        }
 
        else {
            this.io.emit('data', `PLAYEV ${playId} ${args[0]} ${JSON.stringify(args.slice(1))}\n`);
        }
    }
}