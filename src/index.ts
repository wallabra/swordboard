import uuidv4 from 'uuid/v4';
import EventEmitter from 'events';



const PLAYER_FIND_RANGE = 5;
const PLAYER_SEEK_RANGE = 10;
const CHUNK_SIZE = 5;
const CHUNK_ACTV_RANGE = 20;

const MAX_TURN_LEN = 3;


function chunkRange(s: number) {
    return Math.ceil(s / CHUNK_SIZE)
}

export interface PositionLike {
    x: number;
    y: number;
}

export class Position {
    public x: number;
    public y: number;

    constructor(pl: PositionLike) {
        this.x = pl.x;
        this.y = pl.y;
    }

    static make(x: number, y: number) {
        return new this({x: x, y: y});
    }

    addFrom(other: PositionLike) {
        this.x += other.x;
        this.y += other.y;
    }

    subFrom(other: PositionLike) {
        this.x -= other.x;
        this.y -= other.y;
    }

    mulFromVec(other: PositionLike) {
        this.x *= other.x;
        this.y *= other.y;
    }

    mulFromNum(other: number) {
        this.x *= other;
        this.y *= other;
    }
}


export class BoardObject extends EventEmitter {
    public id: string;
    public pos: Position;
    public board: Board;
    public sprite: string;
    public removed: boolean = false;

    
    static load(board: Board, json: string): BoardObject | null {
        let data = JSON.parse(json);

        let c: any = exports[data.type];

        Object.keys(data.options).forEach((ok) => {
            let ov = data.options[ok];

            if (ov && ov['__POS'])
                data.options[ok] = new Position(ov); 
        })

        if (c.prototype instanceof BoardObject) {
            let cons: typeof BoardObject = c;

            return new cons(board, data.options);
        }

        return null;
    }

    myChunkX() {
        return Math.floor(this.pos.x / CHUNK_SIZE);
    }

    myChunkY() {
        return Math.floor(this.pos.y / CHUNK_SIZE);
    }

    myChunkHash() {
        return this.board.chunkHash(Math.floor(this.pos.x / CHUNK_SIZE), Math.floor(this.pos.y / CHUNK_SIZE));
    }

    myChunk() {
        return this.board.chunkAt(Math.floor(this.pos.x / CHUNK_SIZE), Math.floor(this.pos.y / CHUNK_SIZE));
    }

    move(offset: Position) {
        if (this.board.client)
            return;

        let prev: string | undefined = undefined, now: string | undefined = undefined;

        if (!this.board.client) {
            prev = this.myChunkHash();
            now = this.board.chunkHash(Math.floor((this.pos.x + offset.x) / CHUNK_SIZE), Math.floor((this.pos.y + offset.y) / CHUNK_SIZE));

            if (prev != now) {
                this.deactivateAround();
                
                this.board.chunkAtHash(prev).delete(this);
            }
        }
        
        this.pos.addFrom(offset);
        this.netEmit('move', ''+this.pos.x, ''+this.pos.y);
        
        if (!this.board.client && now && prev != now) {
            this.activateAround();
            this.board.chunkAtHash(now).add(this);
        }
    }

    nearbyChunks(callback: (nearbyChunk: Chunk) => void, manhattanDist: number = 3) {
        for (let x = -manhattanDist; x <= manhattanDist; x++) {
            for (let y = -manhattanDist; y <= manhattanDist; y++) {
                callback(this.board.chunkAt(this.myChunkX() + x, this.myChunkY() + y));
            }
        }
    }

    deactivateAround() {
        if (this instanceof BoardLiving && this.player) {
            this.nearbyChunks((ch) => ch.deactivate(), chunkRange(CHUNK_ACTV_RANGE));
        }
    }

    activateAround() {
        if (this instanceof BoardLiving && this.player) {
            this.nearbyChunks((ch) => ch.activate(), chunkRange(CHUNK_ACTV_RANGE));
        }
    }

    moveTo(pos: Position) {
        let offs = new Position(pos);
        offs.subFrom(this.pos);
        
        this.move(offs);
    }

    json() {
        let res = {
            type: this.constructor.name,
            options: {}
        };

        Object.getOwnPropertyNames(this).forEach((pn) => {
            if (pn != 'board' && pn != 'enemy') {
                if ((this as any)[pn] instanceof Position)
                    (res.options as any)[pn] = {
                        x: (this as any)[pn].x,
                        y: (this as any)[pn].y,
                    };

                else
                    (res.options as any)[pn] = (this as any)[pn];
            }
        });

        return JSON.stringify(res);
    }

    netEmit(evt: string, ...args: any[]) {
        let allArguments: [string, ...any[]] = ([evt].concat(args) as any);

        this.emit.apply(this, allArguments);
        this.emit('__NET_EVENT__', this.id, allArguments);
    }

    constructor(board: Board, options: any) {
        super();

        this.board = board;
        this.id = options.id ? options.id.toString() : uuidv4();
        this.pos = new Position(options.pos || new Position({ x: 0, y: 0 }));
        this.sprite = options.sprite || null;

        this.on('move', (x, y) => {
            this.pos = new Position({
                x: +x,
                y: +y,
            });
        });

        if (this.constructor === BoardObject) board.add(this);
    }

    remove() {
        if (!this.board.client) {
            this.deactivateAround();
            this.myChunk().delete(this);
        }

        this.removed = true;

        this.netEmit('remove');
        this.board.emit('deleted object', this.id);
        let res = this.board.objects.delete(this.id);

        if (res) {
            this.board.emit('post deleted object', this.id);
        }

        return res;
    }

    turn(): void {};
    cycle(): Promise<void> | void {};
}


export interface LivingStats {
    attack:     number;
    defense:    number;
    speed:      number;
    luck:       number;
    agility:    number;
    reward:     number;
    accuracy:   number;
}


export interface LivingStatsOptions {
    attack?:    number;
    defense?:   number;
    speed?:     number;
    luck?:      number;
    agility?:   number;
    reward?:    number;
    accuracy?:  number;
}


export interface ProjectileStats {
    damage:     number;
    speed:      number;
    sprite:     string;
}


export interface ProjectileStatsOptions {
    damage?:    number;
    speed?:     number;
    sprite?:    string;
}


export interface AttackStatus {
    status:     string,
    attack:     number,
    defense:    number,
    realDamage: number
};


export class BoardLiving extends BoardObject {
    public health: number;
    public stats: LivingStats;
    public availablePoints: number;
    public name: string;
    public player: boolean = false;
    public projectiles: ProjectileStats | null = null;

    private enemy: string | null = null;
    
    constructor(board: Board, options: any) {
        super(board, options);

        this.health = options.health || 4;
        this.name = options.name || '';
        this.availablePoints = options.availablePoints;
        this.player = !!options.player;
        this.stats = {
            attack:    1,
            defense:   1,
            speed:     1,
            luck:      1,
            agility:   1,
            reward:    1,
            accuracy:  1,
        };

        this.activateAround();

        Object.assign(this.stats, options.stats || {});

        if (options.projectiles) {
            this.projectiles = {
                damage: 1,
                speed: 1,
                sprite: 'projectile.png',
            };

            Object.assign(this.projectiles, options.projectiles || {});
        }

        this.on('set-stats', (stats) => {
            this.stats = JSON.parse(stats);
        });

        this.on('set-health', (heal) => {
            this.health = +heal;
        });

        this.on('set-points', (pts) => {
            this.availablePoints = +pts;
        });

        this.on('move', (x, y) => {
            this.pos = new Position({
                x: +x,
                y: +y
            });
        });

        if (this.constructor === BoardLiving) board.add(this);
    }

    getReward(reward: number) {
        this.availablePoints += Math.round(reward);
        this.netEmit('set-points', [''+this.availablePoints]);
    }

    die(instigator?: BoardLiving) {
        if (instigator) {
            instigator.getReward(this.stats.reward);
        }

        this.health = 0;
        this.netEmit('death', instigator ? instigator!.id : '');
        this.remove();
    }

    takeDamage(damage: number, instigator?: BoardLiving, type: string = '') {
        if (damage > 0) {
            this.health -= damage;
            this.netEmit('damage', ''+damage, type);

            if (this.health <= 0) {
                this.die(instigator);
            }

            else {
                this.netEmit('set-health', ''+this.health);

                if (!this.player && instigator != null) {
                    this.enemy = instigator.id || null;
                }
            }
        }
    }

    tryMoveTo(pos: Position): Position {
        let offs = new Position(pos);
        offs.subFrom(this.pos);
        
        return this.tryMove(offs);
    }

    tryMove(offset: Position): Position {
        if (this.board.client)
            return new Position({x: 0, y: 0});

        offset = new Position(offset);

        if (Math.max(Math.abs(offset.x), Math.abs(offset.y)) > this.stats.speed) {
            if (Math.abs(offset.x) > Math.abs(offset.y)) {
                let ratio = Math.abs(offset.y / offset.x);

                offset.x = Math.round(this.stats.speed * Math.sign(offset.x));
                offset.y = Math.round(this.stats.speed * ratio * Math.sign(offset.y));
            }

            else {
                let ratio = Math.abs(offset.x / offset.y);

                offset.x = Math.round(this.stats.speed * ratio * Math.sign(offset.x));
                offset.y = Math.round(this.stats.speed * Math.sign(offset.y));
            }
        }

        let blocked = false;

        let newPos = new Position(this.pos);
        newPos.addFrom(offset);

        this.nearbyChunks((ch) => {
            if (blocked) return;

            ch.some((o: BoardObject) => {
                if (newPos.x === o.pos.x && newPos.y === o.pos.y && o !== this)
                    return blocked = true;
            });
        }, 1);

        if (!blocked) {
            this.move(offset);
        }

        else {
            offset = new Position({
                x: 0,
                y:0,
            });
        }

        return offset;
    }

    shootAt(other: BoardLiving) {
        if (this.projectiles == null || this.board.client)
            return;

        let offs = new Position(other.pos);
        offs.subFrom(this.pos);

        if (Math.max(Math.abs(offs.x), Math.abs(offs.y)) > 1) {
            if (Math.abs(offs.x) > Math.abs(offs.y)) {
                let ratio = Math.abs(offs.y / offs.x);

                offs.x = Math.round(1 * Math.sign(offs.x));
                offs.y = Math.round(1 * ratio * Math.sign(offs.y));
            }

            else {
                let ratio = Math.abs(offs.x / offs.y);

                offs.x = Math.round(1 * ratio * Math.sign(offs.x));
                offs.y = Math.round(1 * Math.sign(offs.y));
            }
        }

        let pos = new Position(this.pos);

        let proj = new Projectile(this.board, {
            direction: offs,
            damage: this.projectiles!.damage,
            speed: this.projectiles!.speed,
            sprite: this.projectiles!.sprite || 'projectile.png',
            instigator: this,
            pos: pos,
        });
    }

    attack(other: BoardLiving): AttackStatus {
        if (this.health <= 0)
            return {
                status: 'dead',
                attack: 0,
                defense: 0,
                realDamage: 0,
            };

        if (other.health <= 0)
            return {
                status: 'already dead',
                attack: 0,
                defense: 0,
                realDamage: 0,
            };

        let attack = Math.round(Math.random() * this.stats.attack);
        let defense = Math.round(Math.random() * this.stats.defense);

        let dmg = attack - defense;

        let status = 'underwhelmed';

        if (dmg > 0) {
            status = 'hit';

            if (Math.random() ** (1 / Math.log(other.stats.agility) * Math.log(this.stats.accuracy)) > 0.6) {
                dmg = 0;
                status = 'miss';
            }

            if (Math.random() ** (1 / this.stats.luck) > 0.85) {
                dmg = dmg * 2;
                status = 'critical';
            }

            dmg = Math.round(dmg);
            other.takeDamage(dmg, this, status);
        }

        else if (dmg < 0) {
            dmg = 1;

            this.takeDamage(dmg, other, 'defense');
        }

        return {
            status: status,
            attack: attack,
            defense: defense,
            realDamage: dmg,
        };
    }

    cycle(): Promise<void> | void {
        return new Promise((resolve) => {
            if (!this.player && !this.board.client && this.myChunk().isActive()) {
                setTimeout(() => {
                    if (this.enemy != null && this.board.objects.has(this.enemy) && (this.board.objects.get(this.enemy)! instanceof BoardLiving) && (this.board.objects.get(this.enemy)! as BoardLiving).health <= 0)
                        this.enemy = null;

                    let enemy: BoardLiving | null = this.board.objects.get(this.enemy!)! as BoardLiving;

                    if (enemy != null) {
                        let distance = Math.sqrt((enemy.pos.x - this.pos.x) ** 2 + (enemy.pos.y - this.pos.y) ** 2);

                        if (distance > PLAYER_SEEK_RANGE)
                            enemy = null;
                    }

                    if (enemy == null) {
                        let closestPlayer: BoardLiving | null = null;
                        let closestDist = 0;

                        this.nearbyChunks((ch) => {
                            ch.forEach((obj: BoardObject) => {
                                if (obj instanceof BoardLiving && obj.player) {
                                    let distance = Math.sqrt((obj.pos.x - this.pos.x) ** 2 + (obj.pos.y - this.pos.y) ** 2);

                                    if (distance <= PLAYER_FIND_RANGE && (closestPlayer == null || distance < closestDist)) {
                                        closestPlayer = obj;
                                        closestDist = distance;
                                    }
                                }
                            });
                        }, chunkRange(PLAYER_FIND_RANGE));

                        enemy = closestPlayer;
                    }

                    if (enemy != null) {
                        let manhDist = Math.max(Math.abs(this.pos.x - enemy.pos.x), Math.abs(this.pos.y - enemy.pos.y));

                        if (manhDist <= 1)
                            this.attack(enemy);

                        else if (this.projectiles != null && manhDist <= 7 && Math.random() < 0.6)
                            this.shootAt(enemy);

                        else if (Math.random() < 0.8)
                            this.tryMoveTo(enemy.pos);
                    }

                    else if (Math.random() < 0.35) {
                        let randomOffs = new Position({
                            x: Math.random() * 2 - 1,
                            y: Math.random() * 2 - 1
                        });
                        randomOffs.mulFromNum(this.stats.speed);
                        
                        randomOffs.x = Math.round(randomOffs.x);
                        randomOffs.y = Math.round(randomOffs.y);

                        this.move(randomOffs);
                    }

                    this.enemy = enemy ? enemy.id : null;
                    resolve();
                }, Math.random() * 500);
            }

            else resolve();
        });
    } 
}


export class Projectile extends BoardObject {
    public direction: Position;
    public damage: number = 2;
    public speed: number = 1;
    public instigator: string | null = null;

    constructor(board: Board, options: any) {
        super(board, options);

        this.direction = new Position(options.direction || { x: 0, y: 0 });
        this.damage = options.damage || 2;
        this.speed = options.speed || 2;
        this.instigator = options.instigator.id || null;

        if (!this.board.client)
            this.activateAround();

        if (this.constructor === Projectile) board.add(this);
    }

    deactivateAround() {
        this.nearbyChunks((ch) => ch.deactivate(), chunkRange(CHUNK_ACTV_RANGE));
    }

    activateAround() {
        this.nearbyChunks((ch) => ch.activate(), chunkRange(CHUNK_ACTV_RANGE));
    }

    cycle(): Promise<void> | void {
        for (let i = 0; i < this.speed; i++) {
            this.move(this.direction);

            if (this.myChunk().some((e) => {
                if (e.pos.x === this.pos.x && e.pos.y === this.pos.y && e !== this && (this.instigator == null || e !== this.board.objects.get(this.instigator))) {
                    if (e instanceof BoardLiving) {
                        let instig = null;

                        if (this.instigator != null)
                            instig = this.board.objects.get(this.instigator);

                        if (!(instig instanceof BoardLiving))
                            instig = null;

                        e.takeDamage(this.damage, instig || undefined);
                    }

                    this.remove();
                    return true;
                }
            })) break;
        }
    }
}


let defaultPlayerStats: LivingStats = {
    attack:    2,
    defense:   2,
    luck:      2,
    agility:   2,
    reward:    4,
    speed:     2,
    accuracy:  2,
}


export class Chunk {
    public objects: Set<BoardObject> = new Set();
    public active: number = 0;
    

    add(object: BoardObject) {
        this.objects.add(object);
    }

    delete(object: BoardObject) {
        this.objects.delete(object);
    }

    forEach(callback: (value: BoardObject, chunk: Chunk) => void) {
        this.objects.forEach((o) => callback(o, this));
    }

    some(callback: (value: BoardObject, chunk: Chunk) => boolean | void) {
        let a = Array.from(this.objects);

        for (let i = 0; i < a.length; i++) {
            if (callback(a[i], this))
                return true;
        }

        return false;
    }

    every(callback: (value: BoardObject, chunk: Chunk) => boolean | void) {
        let a = Array.from(this.objects);

        for (let i = 0; i < a.length; i++) {
            if (!callback(a[i], this))
                return false;
        }

        return true;
    }

    activate() {
        this.active++;
    }

    deactivate() {
        this.active--;
    }

    isActive() {
        return this.active > 0;
    }
}


export class Player extends EventEmitter {
    public id: string;
    public board: Board;
    public frozen: boolean = false;

    public object?: BoardLiving;


    constructor(board: Board, id: string = uuidv4()) {
        super();

        this.board = board;
        this.id = id;

        this.on('freeze', () => {
            this.frozen = true;
        });

        this.on('unfreeze', () => {
            this.frozen = false;
        });
    }

    freeze() {
        this.frozen = true;
        this.netEmit('freeze');
    }

    unfreeze() {
        this.frozen = false;
        this.netEmit('unfreeze');
    }

    remove() {
        this.netEmit('remove');
        
        if (this.board.players.get(this.board.turns[this.board.turnIndex]) === this)
            this.board.turn();

        while (this.board.turns.indexOf(this.id) != -1) {
            this.board.turns.splice(this.board.turns.indexOf(this.id), 1);
        }

        if (this.object)
            this.object.remove();

        this.board.players.delete(this.id);

        this.board.emit('player removed', this);
    }

    onLogin(name: string) {
        this.object = new BoardLiving(this.board, {
            name: name,
            sprite: 'player.png',
            player: true,

            pos: {
                x: Math.round(Math.random() * 50 - 25),
                y: Math.round(Math.random() * 50 - 25),
            },
        
            health: 3,
            availablePoints: 3,
            stats: JSON.parse(JSON.stringify(defaultPlayerStats)),
        });

        this.object.on('death', () => {
            while (this.board.turns.indexOf(this.id) != -1) {
                this.board.turns.splice(this.board.turns.indexOf(this.id), 1);
            }
        });

        this.board.turns.push(this.id);
        this.board.emit('player login', this);

        if (Array.from(this.board.players.values()).filter((p) => this.board.validPlayer(p)).length > 1)
            this.board.turnIntv = (setTimeout(() => this.board.turn(), MAX_TURN_LEN * 1000) as any);
    }
    
    netEmit(evt: string, ...args: any) {
        let allArguments: [string, ...any[]] = ([evt].concat(args) as any);

        this.emit.apply(this, allArguments);
        this.emit('__NET_EVENT__', this.id, allArguments);
    }

    ownTurn() {
        this.netEmit('turn');
    }

    isTurn() {
        return this === this.board.players.get(this.board.turns[this.board.turnIndex]);
    }

    command(cmd: string, args: string[]) {
        console.log('\rPlayer got command ' + cmd + ' with ' + args.length + ' arguments');
        process.stderr.write('> ');

        if (this.object != null) {
            if (cmd.toUpperCase() === 'ATTACK' && args.length > 0 && this.isTurn()) {
                let battling = this.board.objects.get(args[0]);

                if (battling && battling instanceof BoardLiving) {
                    this.object.attack(battling);

                    this.board.turn();
                }
            }

            else if (cmd.toUpperCase() === 'WAIT' && this.isTurn()) {
                this.board.turn();
            }

            else if (cmd.toUpperCase() === 'RETRY') {
                this.object = undefined;
            }

            else if (cmd.toUpperCase() === 'MOVETO' && args.length > 1 && !args.slice(0, 2).some((x) => isNaN(+x)) && this.isTurn()) {
                let pos = new Position({
                    x: +args[0],
                    y: +args[1],
                });

                let offs = this.object.tryMoveTo(pos);

                if (offs.x !== 0 || offs.y !== 0)
                    this.board.turn();
            }

            else if (cmd.toUpperCase() === 'ADDSTAT' && args.length > 0 && this.object.availablePoints > 0) {
                (this.object.stats as any)[args[0]]++;
                this.object.availablePoints--;

                this.object.netEmit('set-stats', [JSON.stringify(this.object.stats)]);
                this.object.netEmit('set-points', [''+this.object.availablePoints]);
            }

            else if (cmd.toUpperCase() === 'ADDHEALTH' && args.length > 0 && this.object.availablePoints > 0) {
                this.object.health++;
                this.object.availablePoints--;

                this.object.netEmit('set-health', [''+this.object.health]);
                this.object.netEmit('set-points', [''+this.object.availablePoints]);
            }
        }

        else {
            if (cmd.toUpperCase() === 'LOGIN' && args.length > 0) {
                let name = args[0];
                
                this.onLogin(name);
                this.netEmit('login', name, this.object!.id);
            }
        }
    }
}


export interface BoardOptions {
    objects?: Map<string, BoardObject>;
    players?: Map<string, Player>;
    port?: number;
    client?: boolean;
}


export class Board extends EventEmitter {
    public objects: Map<string, BoardObject>;
    public players: Map<string, Player>;
    public turns: string[] = [];
    public turnIndex: number = 0;
    public turnIntv: number = 0;

    public chunks: Map<string, Chunk> = new Map();
    public client: boolean  = false;

    private turnProm: Promise<void> | null = null;


    nearbyChunks(cx: number, cy: number, callback: (nearbyChunk: Chunk) => void, manhattanDist: number = 3) {
        for (let x = -manhattanDist; x <= manhattanDist; x++) {
            for (let y = -manhattanDist; y <= manhattanDist; y++) {
                callback(this.chunkAt(cx + x, cy + y));
            }
        }
    }

    constructor(options: BoardOptions = {}) {
        super();
        
        this.objects = new Map(options.objects ? options.objects.entries() : []);
        this.players = new Map(options.players ? options.players.entries() : []);
        this.client = !!options.client;
    }

    stringHash(s: string) {
        /*
        let hh = createHash('sha256');
        hh.update(s);

        return hh.digest('base64');
        */

        return JSON.stringify(s);
    }

    chunkHash(cx: number, cy: number) {
        let s = (''+cx + ''+cy);
        return this.stringHash(s);
    }

    chunkAtHash(hash: string) {
        if (this.chunks.has(hash))
            return this.chunks.get(hash)!;

        else {
            let s = new Chunk();
            this.chunks.set(hash, s);

            return s;
        }
    }

    chunkAt(cx: number, cy: number) {
        let hash = this.chunkHash(cx, cy);

        return this.chunkAtHash(hash);
    }

    clean() {
        this.objects = new Map();
        this.players = new Map();
        
        this.turnIntv = 0;

        this.turns = [];
        this.turnIndex = 0;
    }

    validPlayer(player?: Player) {
        return player != null && player.object != null && !player.frozen;
    }

    add(object: BoardObject) {
        object.emit('pre-add');

        this.objects.set(object.id, object);
        object.myChunk().add(object);
        this.emit('new object', object);

        object.emit('post-add');
        
        object.netEmit('ADD');
    }

    addPlayer(player: Player) {
        this.players.set(player.id, player);
        this.emit('new player', player);

        player.netEmit('ADD');
    }

    turn() {
        let prom: Promise<void> = new Promise((resolve) => {
            let p = Promise.resolve();

            if (this.turnProm)
                p = this.turnProm;

            p.then(() => {
                this.turnProm = prom;

                if (this.players.has(this.turns[this.turnIndex]))
                    this.players.get(this.turns[this.turnIndex])!.ownTurn();

                if (this.turnIntv)
                    clearInterval(this.turnIntv);

                if (Array.from(this.players.values()).filter((p) => this.validPlayer(p)).length > 1)
                    this.turnIntv = (setTimeout(() => this.turn(), MAX_TURN_LEN * 1000) as any);            

                if (!this.client) {
                    this.objects.forEach((o) => o.turn());
                }

                let history = [this.turnIndex];

                let iter = () => {
                    return new Promise((resolve) => {
                        this.turnIndex++;

                        let proms = [];

                        if (this.turnIndex >= this.turns.length && !this.client) {
                            this.turnIndex = 0;
                            this.objects.forEach((o) => proms.push(Promise.resolve(o.cycle())));
                        }

                        else
                            proms.push(Promise.resolve());

                        Promise.all(proms).then(() => {
                            if ((this.validPlayer(this.players.get(this.turns[this.turnIndex])) && this.players.get(this.turns[this.turnIndex])!.object!.health >= 0) || history.indexOf(this.turnIndex) != -1)
                                resolve();

                            else
                                iter().then(() => {
                                    resolve();
                                });
                        });
                    });
                }

                iter().then(() => {
                    if (this.validPlayer(this.players.get(this.turns[this.turnIndex]))) {
                        console.log(`\rIt's now ${this.players.get(this.turns[this.turnIndex])!.object!.name}'s turn!`);
                        process.stderr.write('> ');
                    }

                    this.emit('turn');

                    this.turnProm = null;
                    resolve();
                });
            });
        });

        return prom;
    }
}
