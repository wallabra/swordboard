declare module "*.ogg" {
    export default string;
}

declare module "*.png" {
    export default string;
}

declare module "*.json" {
    export default any;
}